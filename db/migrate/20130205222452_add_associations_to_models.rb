class AddAssociationsToModels < ActiveRecord::Migration
  def self.up
    add_column :questions, :questionnaire_id, :integer
    add_column :answers, :question_id, :integer
  end

  def self.down
    remove_column :questions, :questionnaire_id
    remove_column :answers, :question_id
  end
end
