class Answer < ActiveRecord::Base
  attr_accessible :name, :question, :question_id

  belongs_to :question
end
