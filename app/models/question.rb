class Question < ActiveRecord::Base
  attr_accessible :name, :answers, :answers_attributes, :questionnaire, :questionnaire_id

  has_many :answers
  belongs_to :questionnaire

  accepts_nested_attributes_for :answers
end
